#ifndef QOLOR_FUNCTION_DRIVER_H__
#define QOLOR_FUNCTION_DRIVER_H__

#include "basic_iterable.h"

namespace qolor
{

namespace internal
{

template<typename FuncT, typename ConditionT>
class custom_func_iterator
{
private:
	using func_type = typename std::decay<FuncT>::type;
	using ret_type = decltype(std::declval<func_type>()());
	using condition_type = typename std::decay<ConditionT>::type;

public:
	using value_type = typename std::decay<ret_type>::type;
	using difference_type = typename utils::comb_any<std::ptrdiff_t, value_type>::type;
	using reference = value_type const&;
	using pointer = value_type const*;
	using iterator_category = std::input_iterator_tag;

private:
	func_type func_;
	condition_type cond_;
	value_type buf_;
	bool cond_result_;
	bool needs_fetch_;

	using cond_ret_type = typename std::decay<decltype(cond_(buf_))>::type;

	static_assert(std::is_arithmetic<cond_ret_type>::value, "The return type of Condition must be bool.");

public:
	custom_func_iterator() = delete;
	custom_func_iterator(custom_func_iterator const&) = default;
	custom_func_iterator(custom_func_iterator&&) = default;

	template<typename F, typename C>
	custom_func_iterator(F&& f, C&& c, const bool& do_iterate)
		: func_(std::forward<F>(f)), cond_(std::forward<C>(c)),
		cond_result_(do_iterate), needs_fetch_(do_iterate) {}

	custom_func_iterator & operator++() {
		if (cond_result_) {
			buf_ = func_();
			cond_result_ = cond_(buf_);
			needs_fetch_ = false;
		}
		return *this;
	}

	custom_func_iterator operator++(int) {
		custom_func_iterator i(*this);
		++(*this);
		return std::move(i);
	}
	
	bool operator==(custom_func_iterator const& o) const { return cond_result_ == o.cond_result_; }
	bool operator!=(custom_func_iterator const& o) const { return cond_result_ != o.cond_result_; }

	value_type operator*() {
		if (needs_fetch_) ++(*this);
		return buf_;
	}

	std::unique_ptr<value_type> operator->() {
		return std::unique_ptr<value_type>(new value_type(std::forward<ret_type>(buf_)));
	}
};

} // namespace internal


template<typename Func, typename Condition>
typename std::enable_if<
	(utils::has_func_operator<Func>::value && utils::has_func_operator<Condition>::value)
	&& (!utils::is_iterator<Func>::value || !utils::is_iterator<Condition>::value)
	,internal::iterable<internal::custom_func_iterator<Func, Condition>>
>::type from(Func&& f, Condition&& c)
{
	using ftype = typename std::decay<Func>::type;
	using ctype = typename std::decay<Condition>::type;
	using iter_t = internal::custom_func_iterator<ftype,ctype>;
	using vtype = typename internal::custom_func_iterator<ftype,ctype>::value_type;
	using bool_t = decltype(c(std::declval<vtype>()));
		//utils::has_func_operator<ftype>

	iter_t b(f, c, true);
	iter_t e(f, c, false);
	return internal::iterable<internal::custom_func_iterator<Func,Condition>>(std::move(b), std::move(e));
}


} // namespace qolor

#endif // QOLOR_FUNCTION_DRIVER_H__

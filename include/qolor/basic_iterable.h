#ifndef QOLOR_BASIC_ITERABLE_H__
#define QOLOR_BASIC_ITERABLE_H__

#include "utilities.h"
#include <memory>
#include <vector>

namespace qolor
{

namespace internal
{

template <typename InnerType, typename FuncType>
class select_iterator
{
private:
	using inner_type = typename std::decay<InnerType>::type;
	using func_type = typename std::decay<FuncType>::type;

	inner_type cur_;
	func_type func_;
	
public:
	using difference_type = typename std::iterator_traits<inner_type>::difference_type;
	using value_type = typename std::decay<decltype(func_(*cur_))>::type;
	using reference = typename std::add_rvalue_reference<value_type>::type;
	using pointer = typename std::add_pointer<value_type>::type;
	using iterator_category = std::input_iterator_tag;

	select_iterator() = delete;
	select_iterator(select_iterator const&) = default;
	select_iterator(select_iterator&&) = default;

	template <typename I, typename F>
	select_iterator(I&& it, F&& f) : cur_(std::forward<I>(it)), func_(std::forward<F>(f)) {}

	bool operator==(inner_type const& o) const { return cur_ == o; }
	bool operator!=(inner_type const& o) const { return cur_ != o; }
	bool operator==(select_iterator const& o) const { return cur_ == o.cur_; }
	bool operator!=(select_iterator const& o) const { return cur_ != o.cur_; }

	select_iterator & operator++() { ++cur_; return *this; }
	select_iterator operator++(int) { return select_iterator(cur_++, func_); }

	value_type operator*() { return std::move(func_(*cur_)); }

	std::unique_ptr<value_type> operator->() {
		return std::unique_ptr<value_type>(new value_type(std::move(func_(*cur_))));
	}
};


template <typename InnerType, typename FuncType>
class where_iterator
{
private:
	using inner_type = typename std::decay<InnerType>::type;
	using inner_traits = std::iterator_traits<inner_type>;
	using func_type = typename std::decay<FuncType>::type;

	inner_type cur_, end_;
	func_type func_;

public:
	using difference_type = typename inner_traits::difference_type;
	using value_type = typename inner_traits::value_type;
	using reference = typename inner_traits::reference;
	using pointer = typename inner_traits::pointer;
	using iterator_category = typename utils::get_bidirectional_tag<typename inner_traits::iterator_category>::type;

	where_iterator() = delete;
	where_iterator(where_iterator const&) = default;
	where_iterator(where_iterator&&) = default;

	template <typename B, typename E, typename F>
	where_iterator(B&& b, E&& e, F&& f)
		: cur_(std::forward<B>(b)), end_(std::forward<E>(e)), func_(std::forward<F>(f)) {
		while (cur_ != end_ && !func_(*cur_)) ++cur_;
	}

	where_iterator & operator++() {
		do ++cur_; while (cur_ != end_ && !func_(*cur_));
		return *this;
	}

	where_iterator & operator--() {
		do --cur_; while (!func_(*cur_));
		return *this;
	}

	where_iterator operator++(int) {
		where_iterator w(*this);
		++(*this);
		return std::move(w);
	}

	where_iterator operator--(int) {
		where_iterator w(*this);
		--(*this);
		return std::move(w);
	}

	bool operator==(inner_type const& o) const { return cur_ == o; }
	bool operator!=(inner_type const& o) const { return cur_ != o; }
	bool operator==(where_iterator const& o) const { return cur_ == o.cur_; }
	bool operator!=(where_iterator const& o) const { return cur_ != o.cur_; }

	auto operator*() -> decltype(*cur_) { return *cur_; }

	std::unique_ptr<value_type> operator->() {
		return std::unique_ptr<value_type>(new value_type(*cur_));
	}
};


template <typename InnerType1, typename InnerType2, typename Predicate>
class join_iterator
{
private:
	using inner_type1 = typename std::decay<InnerType1>::type;
	using inner_type2 = typename std::decay<InnerType2>::type;
	using predicate = typename std::decay<Predicate>::type;
	using value_type1 = typename std::iterator_traits<inner_type1>::value_type;
	using value_type2 = typename std::iterator_traits<inner_type2>::value_type;

	inner_type1 cur1_, end1_;
	inner_type2 begin2_, end2_, cur2_;
	predicate pred_;

	static const bool is_input1 = std::is_same<
		typename std::iterator_traits<inner_type1>::iterator_category,
		std::input_iterator_tag
	>::value;

	static const bool is_input2 = std::is_same<
		typename std::iterator_traits<inner_type2>::iterator_category,
		std::input_iterator_tag
	>::value;

	//typename std::enable_if<is_input1, value_type1>::type value1_;

	static_assert(!is_input2, "Second iterator cannot be an input iterator. You may want to use iterable<>::to_vector().");
	
public:
	using difference_type = typename inner_type1::difference_type;
	using value_type = std::pair<value_type1, value_type2>;
	using reference = typename std::add_rvalue_reference<value_type>::type;
	using pointer = typename std::add_pointer<value_type>::type;
	using iterator_category = std::input_iterator_tag;

	join_iterator() = delete;
	join_iterator(join_iterator const&) = default;
	join_iterator(join_iterator&&) = default;

	template <typename I1, typename I2, typename P>
	join_iterator(I1&& begin1, I1&& end1, I2&& begin2, I2&& end2, P&& pred)
		: cur1_(std::forward<I1>(begin1)), end1_(std::forward<I1>(end1)),
		begin2_(std::forward<I2>(begin2)), end2_(std::forward<I2>(end2)),
		pred_(std::forward<P>(pred)) {
		cur2_ = begin2_;
	}

	bool operator==(join_iterator const& o) const { return (cur1_ == o.cur1_) && (cur2_ == o.cur2_); }
	bool operator!=(join_iterator const& o) const { return (cur1_ != o.cur1_) || (cur2_ != o.cur2_); }
	
	typename std::enable_if<!is_input1, join_iterator &>::type operator++() {
		do {
			++cur2_;
			while ((cur2_ == end2_) && (cur1_ != end1_)) {
				++cur1_;
				cur2_ = begin2_;
			}
		} while (cur1_ != end1_ && !pred_(*cur1_,*cur2_));
		return *this;
	}

	/*typename std::enable_if<is_input1, join_iterator &>::type operator++() {
		bool check_q;
		do {
			++cur2_;
			check_q = (cur1_ != end1_);
			while ((cur2_ == end2_) && check_q) {
				check_q = ((++cur1_) != end1_);
				if (check_q) {
					value1_ = *cur1_;
					cur2_ = begin2_;
				}
			}
		} while (check_q && !pred_(value1_, *cur2_));
		return *this;
	}*/

	join_iterator operator++(int) {
		join_iterator ret(static_cast<join_iterator const&>(*this));
		++(*this);
		return std::move(ret);
	}

	typename std::enable_if<!is_input1, value_type>::type operator*() { return value_type(*cur1_, *cur2_); }
	//typename std::enable_if<is_input1, value_type>::type operator*() { return value_type(value1_, *cur2_); }

	std::unique_ptr<value_type> operator->() {
		return std::unique_ptr<value_type>(new value_type(*cur1_, *cur2_));
	}
};


template <typename InnerType>
class limited_iterator
{
private:
	using inner_type = typename std::decay<InnerType>::type;
	using inner_traits = std::iterator_traits<inner_type>;

	inner_type cur_, end_;
	size_t left_;

public:
	using difference_type = typename inner_traits::difference_type;
	using value_type = typename inner_traits::value_type;
	using reference = typename inner_traits::reference;
	using pointer = typename inner_traits::pointer;
	using iterator_category = std::input_iterator_tag;

	limited_iterator() = delete;
	limited_iterator(limited_iterator const&) = default;
	limited_iterator(limited_iterator&&) = default;

	template <typename B, typename E, typename F>
	limited_iterator(B&& b, E&& e, size_t const& num)
		: cur_(std::forward<B>(b)), end_(std::forward<E>(e)), left_(0) {
		if (num) left_ = num - 1;
		else cur_ = end_;
	}

	limited_iterator & operator++() {
		if (left_) { ++cur_; --left_; }
		else cur_ = end_;
		return *this;
	}

	limited_iterator operator++(int) {
		limited_iterator w(*this);
		++(*this);
		return std::move(w);
	}

	bool operator==(inner_type const& o) const { return cur_ == o; }
	bool operator!=(inner_type const& o) const { return cur_ != o; }
	bool operator==(limited_iterator const& o) const { return cur_ == o.cur_; }
	bool operator!=(limited_iterator const& o) const { return cur_ != o.cur_; }

	value_type operator*() { return *cur_; }

	std::unique_ptr<value_type> operator->() {
		return std::unique_ptr<value_type>(new value_type(*cur_));
	}
};


template <typename IteratorType>
class iterable
{
public:
	using iterator = typename std::decay<IteratorType>::type;
	using value_type = typename std::iterator_traits<iterator>::value_type;
	using reference = typename std::iterator_traits<iterator>::reference;
	using const_value_type = typename std::add_const<value_type>::type;
	using const_reference = typename std::add_lvalue_reference<const_value_type>::type;

private:
	iterator begin_, end_;

public:
	iterable() = delete;
	iterable(iterable const&) = default;
	iterable(iterable&&) = default;

	template<typename B, typename E>
	iterable(B&& b, E&& e) : begin_(std::forward<B>(b)), end_(std::forward<E>(e)) {}

	iterator const & begin() const { return begin_; }
	iterator const & end() const { return end_; }

	bool empty() const { return begin_ == end_; }

	template <typename F>
	iterable<select_iterator<iterator, F>> select(F&& f) {
		using iter_t = select_iterator<iterator, F>;
		iter_t b(begin_, std::forward<F>(f)), e(end_, std::forward<F>(f));
		return iterable<iter_t>(std::move(b), std::move(e));
	}

	template <typename F>
	iterable<where_iterator<iterator, F>> where(F&& f) {
		using iter_t = where_iterator<iterator, F>;
		iter_t b(begin_, end_, std::forward<F>(f));
		iter_t e(end_, end_, std::forward<F>(f));
		return iterable<iter_t>(std::move(b), std::move(e));
	}

	iterable<limited_iterator<iterator>> take(size_t const& count) {
		using iter_t = limited_iterator<iterator>;
		return iterable<iter_t>(iter_t(begin_, count), iter_t(end_, 0));
	}

	iterable<iterator> skip(size_t const& count) {
		iterator i(begin_);
		utils::iterator_utils<iterator, typename std::iterator_traits<iterator>::iterator_category>::advance(i, end_, count);
		return iterable<iterator>(std::move(i), end_);
	}

	value_type first() {
		if (empty()) return value_type();
		return std::forward<value_type>(*begin_);
	}

	value_type last() {
		value_type retval;
		for (auto v : *this) retval = v;
		return std::move(retval);
	}

	template<typename J, typename P>
	join_iterator<iterator, J, P> join(iterable<J>&& o, P&& pred) {
		using rtype = join_iterator<iterator, J, P>;
		return rtype(begin_, end_, std::move(o.begin()), std::move(o.end()), std::forward<P>(pred));
	}

	template <typename F>
	bool contains(value_type const& value, F&& f) {
		for (auto v : *this)
			if (f(value,v)) return true;
		return false;
	}

	bool contains(value_type const& value) {
		for (auto v : *this)
			if (value == v) return true;
		return false;
	}

	template <typename F>
	value_type aggregate(F&& f) {
		static_assert(std::is_trivial<value_type>::value || std::is_default_constructible<value_type>::value,
			"value_type must be default constructible.");
		if (begin_ == end_) return value_type();
		iterator i(begin_);
		value_type acc(*i);
		while ((++i) != end_) acc = f(acc, *i);
		return std::move(acc);
	}

	value_type sum() {
		typedef value_type const& ref;
		return aggregate([&](ref a, ref b){ return a + b; });
		//long double ret = 0;
		//for (auto v : *this) ret += v;
		//return std::move(ret);
	}

	std::vector<value_type> to_vector() {
		std::vector<value_type> ret;
		for (auto v : *this) ret.push_back(v);
		return std::move(ret);
	}
};


} // namespace internal


template<typename IteratorType1, typename IteratorType2>
typename std::enable_if<
	utils::is_same_decay<IteratorType1,IteratorType2>::value
	&& utils::is_iterator<IteratorType1>::value
	&& utils::is_iterator<IteratorType2>::value
	,internal::iterable<typename std::decay<IteratorType1>::type>
>::type from(IteratorType1&& begin, IteratorType2&& end)
{
	using t1 = typename std::decay<IteratorType1>::type;
	return internal::iterable<t1>(std::forward<IteratorType1>(begin), std::forward<IteratorType2>(end));
}


template<typename ValueT>
internal::iterable<ValueT*> from(ValueT* const& begin, typename std::iterator_traits<ValueT*>::difference_type const& n)
{
	return internal::iterable<ValueT*>(begin, begin + n);
}


template<typename ValueT1, typename ValueT2>
typename std::enable_if<
	utils::is_same_decay<ValueT1,ValueT2>::value
	,internal::iterable<ValueT1*>
>::type from(ValueT1* const& begin, ValueT2* const& end)
{
	return internal::iterable<ValueT1*>(begin, begin + (end - begin));
}


template<typename IterableType>
auto from(IterableType&& c) -> typename std::enable_if<
	utils::is_iterable<IterableType>::value
	,internal::iterable<decltype(c.begin())>
>::type
{
	return internal::iterable<decltype(c.begin())>(c.begin(), c.end());
}


} // namespace qolor

#endif // QOLOR_BASIC_ITERABLE_H__

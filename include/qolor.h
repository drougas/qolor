#ifndef QOLOR_H__
#define	QOLOR_H__

#include "qolor/utilities.h"
#include "qolor/basic_iterable.h"
#include "qolor/function_driver.h"
#include "qolor/range_driver.h"

#ifndef NDEBUG
#include "qolor/debug.h"
#endif

#endif // QOLOR_H__
